import 'package:ecommerce/MyCoupon.dart';
import 'package:ecommerce/MyOrder.dart';
import 'package:ecommerce/components/ColorScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'Cart.dart';
import 'Details_Page.dart';
import 'Product_Listing.dart';
import 'WishList.dart';
import 'public/login.dart';

class AccountInfo extends StatefulWidget {
  const AccountInfo({Key? key}) : super(key: key);

  @override
  _AccountInfoState createState() => _AccountInfoState();
}

class _AccountInfoState extends State<AccountInfo> {
  FirebaseAuth? auth;
  User? _user;
  String? phoneNo;

  @override
  void initState() {
    super.initState();
    auth = FirebaseAuth.instance;
    _user = auth?.currentUser;
    print(_user?.phoneNumber);
    if(_user==null){
      phoneNo='not Logged In yet';
    }
    else{
      phoneNo=_user?.phoneNumber.toString();
    }
  }
  final _auth=FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          Container(
            margin: EdgeInsets.only(right: 20),
            child: Row(
              children: [
                Container(
                  child: GestureDetector(
                    onTap: () {},
                    child: Icon(Icons.search),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Container(
                  child: GestureDetector(
                    onTap: () {},
                    child: Icon(
                      Icons.shopping_cart,
                      color: MyColor.customblack,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
        title: Text(
          "My Account",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: Container(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: MyColor.customblue,
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage("image/profile_dp.jpg"),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    child: Text(
                      "Full Name",
                      style: TextStyle(
                          color: MyColor.customwhite,
                          fontSize: 25,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    child: Text("User: " + phoneNo!,
                      style: TextStyle(
                        color: MyColor.customwhite,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(width: 20),
                      Container(
                        child: Text(
                          "your.name@example.com",
                          style: TextStyle(
                            color: MyColor.customwhite,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 15),
                        child: InkWell(
                          radius: 20,
                          onTap: () {},
                          child: Icon(
                            Icons.edit,
                            color: MyColor.customwhite,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                ],
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyOrder()));
              },
              child: Container(
                margin: EdgeInsets.only(left: 10,right: 10),

                height: 50,
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: MyColor.customash)
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(

                            child: Icon(
                              Icons.store,
                              color: MyColor.customblue,
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            child: Text(
                              "My Orders",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Icon(Icons.arrow_right)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => WishList()));
              },
              child: Container(
                margin: EdgeInsets.only(left: 10,right: 10),

                height: 50,
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: MyColor.customash))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.favorite_border,
                              color: MyColor.customblue,
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            child: Text(
                              "My Wishlist",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Icon(Icons.arrow_right)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyCoupon()));
              },
              child: Container(
                margin: EdgeInsets.only(left: 10,right: 10),

                height: 50,
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: MyColor.customash))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.card_giftcard_outlined,
                              color: MyColor.customblue,
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            child: Text(
                              "My Coupons",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Icon(Icons.arrow_right)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
              },
              child: Container(
                margin: EdgeInsets.only(left: 10,right: 10),

                height: 50,
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: MyColor.customash))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.add_shopping_cart_outlined,
                              color: Colors.blue,
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            child: Text(
                              "My Cart",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Icon(Icons.arrow_right)
                  ],
                ),
              ),
            ),

            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => ProductList()));
              },
              child: Container(
                margin: EdgeInsets.only(left: 10,right: 10),

                height: 50,
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: MyColor.customash))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.list,
                              color: MyColor.customblue,
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            child: Text(
                              "Product List",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Icon(Icons.arrow_right)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: ()async{
                await _auth.signOut();
                Fluttertoast.showToast(
                    msg: "Successfully Logged Out",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black,
                    textColor: Colors.white,
                    fontSize: 16.0);
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
              },
              child: Container(
                margin: EdgeInsets.only(left: 10,right: 10),

                height: 50,
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: MyColor.customash))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Text(
                        "Log Out",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Container(
                      child: Icon(
                        Icons.logout,
                        color: MyColor.customblack,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20,),
            Container(
              alignment: Alignment.center,
              child: Text(
                "Version : 1.0.0",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
