import 'package:ecommerce/Details_Page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'AccountInfo.dart';
import 'Product_Listing.dart';
import 'components/ColorScreen.dart';
import 'home_page.dart';
import 'public/login.dart';

class LayoutPage extends StatefulWidget {
  const LayoutPage({Key? key}) : super(key: key);

  @override
  _LayoutPageState createState() => _LayoutPageState();
}

class _LayoutPageState extends State<LayoutPage> {
  FirebaseAuth? _auth;
  User? _user;

  @override
  void initState() {
    super.initState();
    _auth = FirebaseAuth.instance;
    _user = _auth?.currentUser;
    print("------------- $_user");
  }

  int currentindex=1;

  final screens = [
    AccountInfo(),
    HomePage(),
    ProductList(),
    LoginPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentindex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: MyColor.customblue,
        selectedItemColor: MyColor.customwhite,
        unselectedItemColor: MyColor.customblack,
        // selectedFontSize: 25,
        // unselectedFontSize: 15,
        // iconSize: 10,
        showUnselectedLabels: false,
        // showSelectedLabels: true,
        currentIndex: currentindex,
        onTap: (index) => setState(() => currentindex = index),
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'My Account',
            // backgroundColor: Colors.blue,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            // backgroundColor: Colors.blue,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Product List',
            // backgroundColor: Colors.yellow,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.login),
            label: 'Logged In',
            // backgroundColor: Colors.blue,
          ),
        ],
      ),
    );
  }
}
