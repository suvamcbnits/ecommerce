import 'package:ecommerce/home_page.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:ecommerce/components/ColorScreen.dart';
import "package:http/http.dart" as http;

class Electronics extends StatefulWidget {
  const Electronics({Key? key}) : super(key: key);

  @override
  _ElectronicsState createState() => _ElectronicsState();
}

class _ElectronicsState extends State<Electronics> {
  List<Product> items = [];

  Future<List<Product>> getProducts() async {
    var data = await http.get(Uri.parse("https://fakestoreapi.com/products"));
    var jsondata = json.decode(data.body);
    for (int i = 0; i < jsondata.length; i++) {
      Product pr = Product(
        title: jsondata[i]["title"],
        price: jsondata[i]["price"].toString(),
        description: jsondata[i]["description"],
        category: jsondata[i]["category"],
        image: jsondata[i]["image"],
        rate: jsondata[i]["rating"]["rate"].toString(),
        count: jsondata[i]["rating"]["count"].toString(),
      );
      if (jsondata[i]["category"] == "electronics") {
        items.add(pr);
      }
    }
    print("-------------${items.length}");
    return items;
  }

  bool loading = true;

  void initState() {
    getProducts().whenComplete(() {
      setState(() {
        loading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: LinearProgressIndicator(
                  backgroundColor: MyColor.customblack,
                  valueColor: AlwaysStoppedAnimation(MyColor.customblue)))
          : Container(
              child: GridView.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 2.0,
                  mainAxisSpacing: 2.0,
                  children: List.generate(items.length, (index) {
                    return Card(
                      elevation: 10,
                      shadowColor: MyColor.customblue,
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 8, top: 5),
                              alignment: Alignment.topRight,
                              child: Icon(
                                Icons.favorite,
                                size: 16,
                                color: Colors.red,
                              ),
                            ),
                            Container(
                              height: 130,
                              child: Image.network(items[index].image!),
                            ),
                            Container(
                              width: 170,
                              color: Colors.grey[300],
                              // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Container(
                                    child: RichText(
                                      text: TextSpan(
                                          text: "Rs. " + items[index].price!,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customblack,
                                              fontSize: 12),
                                          children: [
                                            TextSpan(
                                              text: "2̶4̶9̶",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  color: MyColor.customash,
                                                  fontSize: 10),
                                            ),
                                            TextSpan(
                                              text: "  30% off",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: MyColor.customgreen,
                                                  fontSize: 10),
                                            )
                                          ]),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  })),
            ),
    );
  }
}
