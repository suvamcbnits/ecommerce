import 'dart:async';
import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/material.dart';
import 'layout.dart';
class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 3),
            () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LayoutPage())));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: MyColor.customblue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // SizedBox(height: 140,),
            Container(
              alignment: Alignment.bottomCenter,
              height: 180,
              child: Image.asset("image/shop.png"),
            ),
            Container(
                margin: EdgeInsets.all(20),
                child: CircularProgressIndicator(
                  backgroundColor: MyColor.customblack,
                  valueColor: AlwaysStoppedAnimation(MyColor.customwhite),
                )
            ),
          ],
        ),
      ),
    );
  }
}
