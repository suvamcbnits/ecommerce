import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/material.dart';

class Notifications extends StatelessWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notifications"),
      ),
      body: Container(
        child: ListView(
          children: [
            Card(
              elevation: 10,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 76,
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.wallet_giftcard,
                            color: MyColor.customblue,
                            size: 25,
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  "Order Delivered! 😁",
                                  style: TextStyle(
                                      fontSize: 18, fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 7,),
                              Container(
                                child: Text(
                                  "Your E-Shop Order Containing\nrealme Buds Air 2 with Active..\nhas been delivered.",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: MyColor.customash,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(height: 5,),
                              Container(
                                child: Text(
                                  "5 Days ago",
                                  style: TextStyle(
                                    fontSize: 8,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 60,
                          child: Image.asset("image/5.jpeg"),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 5),
                    height: 35,
                    child: Row(
                      children: [
                        Container(
                          width: 175,
                          child: RaisedButton(
                          onPressed: ()=>null,
                            child: Text("Know More"),
                            textColor: MyColor.customblue,
                            color: MyColor.customwhite,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                                side: BorderSide(color: MyColor.customblack))
                        ),),
                        Container(
                          width: 175,
                          child: RaisedButton(
                            onPressed: ()=>null,
                            child: Text("Share Feedback"),
                            textColor: MyColor.customblue,
                            color: MyColor.customwhite,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                                side: BorderSide(color: MyColor.customblack))
                        ),),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Card(
              elevation: 10,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 76,
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.info,
                            color: MyColor.customblue,
                            size: 25,
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  "Liked What you bought?",
                                  style: TextStyle(
                                      fontSize: 18, fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 7,),
                              Container(
                                child: Text(
                                  "Tell us about the Noise Smartwatch\nColorfit Qube SpO2 CharCoal Black..\nhas been delivered.",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: MyColor.customash,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(height: 5,),
                              Container(
                                child: Text(
                                  "24th January",
                                  style: TextStyle(
                                    fontSize: 8,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 60,
                          child: Image.asset("image/notify.png"),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Card(
              elevation: 10,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 76,
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.wallet_giftcard,
                            color: MyColor.customblue,
                            size: 25,
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  "Order Delivered! 😁",
                                  style: TextStyle(
                                      fontSize: 18, fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 7,),
                              Container(
                                child: Text(
                                  "Your E-Shop Order Containing Noise \nColorfit Qube SpO2 CharCoal Black..\nhas been delivered.",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: MyColor.customash,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(height: 5,),
                              Container(
                                child: Text(
                                  "22 January",
                                  style: TextStyle(
                                    fontSize: 8,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 60,
                          child: Image.asset("image/14.jpeg"),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 5),
                    height: 35,
                    child: Row(
                      children: [
                        Container(
                          width: 175,
                          child: RaisedButton(
                              onPressed: ()=>null,
                              child: Text("Know More"),
                              textColor: MyColor.customblue,
                              color: MyColor.customwhite,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3),
                                  side: BorderSide(color: MyColor.customblack))
                          ),),
                        Container(
                          width: 175,
                          child: RaisedButton(
                              onPressed: ()=>null,
                              child: Text("Share Feedback"),
                              textColor: MyColor.customblue,
                              color: MyColor.customwhite,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3),
                                  side: BorderSide(color: MyColor.customblack))
                          ),),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Card(
              elevation: 10,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 76,
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.star,
                            color: MyColor.customblue,
                            size: 25,
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  "Make this Holi more Colorful 😃",
                                  style: TextStyle(
                                      fontSize: 16, fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 7,),
                              Container(
                                child: Text(
                                  "From ₹99 | Grab Great Discounts on Top\nBrands Like Libas,Lakme,Poco,Wildcraft \n& more...",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: MyColor.customash,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(height: 5,),
                              Container(
                                child: Text(
                                  "Show Details",
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: MyColor.customblue
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 60,
                          child: Image.asset("image/sale.jpg"),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
