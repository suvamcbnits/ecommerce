import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/material.dart';

class Address extends StatefulWidget {
  const Address({Key? key}) : super(key: key);

  @override
  _AddressState createState() => _AddressState();
}

class _AddressState extends State<Address> {
  int _value = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          SliverAppBar(
            title: Text("Address"),
          )
        ],
        body: ListView(children: [
          Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10,top: 20),
                  child: Container(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 20),
                              child: CircleAvatar(
                                radius: 15,
                                backgroundColor: MyColor.customblue,
                                child: Text("1"),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 20),
                              child: Text("Address"),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 50,
                          child: Icon(
                            Icons.trending_flat_outlined,
                            color: MyColor.customash,
                            size: 30,
                          ),
                        ),
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: CircleAvatar(
                                radius: 15,
                                child: CircleAvatar(
                                  radius: 13,
                                  backgroundColor: MyColor.customwhite,
                                  child: Text("2"),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Text("Order Summary"),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 50,
                          child: Icon(
                            Icons.trending_flat_outlined,
                            color: MyColor.customash,
                            size: 30,
                          ),
                        ),
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: CircleAvatar(
                                radius: 15,
                                child: CircleAvatar(
                                  radius: 13,
                                  backgroundColor: MyColor.customwhite,
                                  child: Text("3"),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Text("Payment"),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 3,
                  child: Container(
                    color: MyColor.customash,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Text(
                        "+",
                        style: TextStyle(
                            color: MyColor.customblue,
                            fontWeight: FontWeight.bold,
                            fontSize: 30),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Add a new address",
                        style: TextStyle(
                            color: MyColor.customblue,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 3,
                  child: Container(
                    color: MyColor.customash,
                  ),
                ),
                Container(
                  child: Container(
                    child: Row(
                      children: [
                        Radio(
                          value: 1,
                          onChanged: (value) {
                            setState(() {
                              _value = value as int;
                            });
                          },
                          groupValue: _value,
                        ),
                        Container(
                          child: Container(
                            padding: EdgeInsets.only(top: 15, bottom: 15),
                            alignment: Alignment.centerLeft,
                            child: RichText(
                              text: TextSpan(
                                  text: "Full Name",
                                  style: TextStyle(
                                      fontSize: 19, color: MyColor.customblack),
                                  children: [
                                    TextSpan(
                                      text:
                                          "\n\nFull Address,House No.Area,Landmark\nDistrict,Area Pincode-xxxxxx\n+91 xxxxxxxxxx",
                                      style: TextStyle(
                                          color: MyColor.customblack,
                                          fontSize: 17),
                                    )
                                  ]),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 3,
                  child: Container(
                    color: MyColor.customash,
                  ),
                ),
                Container(
                  child: Container(
                    child: Row(
                      children: [
                        Radio(
                          value: 2,
                          onChanged: (value) {
                            setState(() {
                              _value = value as int;
                            });
                          },
                          groupValue: _value,
                        ),
                        Container(
                          child: Container(
                            padding: EdgeInsets.only(top: 15, bottom: 15),
                            alignment: Alignment.centerLeft,
                            child: RichText(
                              text: TextSpan(
                                  text: "Full Name 1",
                                  style: TextStyle(
                                      fontSize: 19, color: MyColor.customblack),
                                  children: [
                                    TextSpan(
                                      text:
                                          "\n\nFull Address 1,House No.Area,Landmark\nDistrict,Area Pincode-xxxxxx\n+91 xxxxxxxxxx",
                                      style: TextStyle(
                                          color: MyColor.customblack,
                                          fontSize: 17),
                                    )
                                  ]),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 3,
                  child: Container(
                    color: MyColor.customash,
                  ),
                ),
                Container(
                  child: Container(
                    child: Row(
                      children: [
                        Radio(
                          value: 3,
                          onChanged: (value) {
                            setState(() {
                              _value = value as int;
                            });
                          },
                          groupValue: _value,
                        ),
                        Container(
                          child: Container(
                            padding: EdgeInsets.only(top: 15, bottom: 15),
                            alignment: Alignment.centerLeft,
                            child: RichText(
                              text: TextSpan(
                                  text: "Full Name 2",
                                  style: TextStyle(
                                      fontSize: 19, color: MyColor.customblack),
                                  children: [
                                    TextSpan(
                                      text:
                                      "\n\nFull Address 2,House No.Area,Landmark\nDistrict,Area Pincode-xxxxxx\n+91 xxxxxxxxxx",
                                      style: TextStyle(
                                          color: MyColor.customblack,
                                          fontSize: 17),
                                    )
                                  ]),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(5.0),
        child: RaisedButton(
          onPressed: () {
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => Address()));
          },
          color: MyColor.customblue,
          textColor: MyColor.customwhite,
          child: Text('DELIVER HERE'),
        ),
      ),
    );
  }
}
