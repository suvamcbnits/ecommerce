import 'dart:convert';
import 'package:ecommerce/Details_Page.dart';
import 'package:ecommerce/Notifications.dart';
import 'package:flutter/material.dart';
import "package:http/http.dart" as http;

import 'Cart.dart';
import 'components/ColorScreen.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool loading = true;
  List<Product> items = [];

  Future<List<Product>> getProducts() async {
    var data = await http.get(Uri.parse("https://fakestoreapi.com/products"));
    var jsondata = json.decode(data.body);
    for (int i = 0; i < jsondata.length; i++) {
      Product pr = Product(
        title: jsondata[i]["title"],
        price: jsondata[i]["price"].toString(),
        description: jsondata[i]["description"],
        category: jsondata[i]["category"],
        image: jsondata[i]["image"],
        rate: jsondata[i]["rating"]["rate"].toString(),
        count: jsondata[i]["rating"]["count"].toString(),
      );
      items.add(pr);
    }
    print("-------------${items.length}");
    return items;
  }

  @override
  void initState() {
    getProducts().whenComplete(() {
      setState(() {
        loading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getProducts();
    return Scaffold(
      drawer: Drawer(),
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                  backgroundColor: MyColor.customblack,
                  valueColor: AlwaysStoppedAnimation(MyColor.customblue)))
          : CustomScrollView(
              slivers: [
                SliverAppBar(
                  floating: true,
                  pinned: true,
                  snap: false,
                  centerTitle: false,
                  // leading: IconButton(
                  //       onPressed: (){},
                  //       icon: Icon(Icons.dehaze),
                  //     ),
                  titleSpacing: -15,
                  title: Row(
                    children: [
                      Container(
                        child: Image.asset("image/shop.png", width: 100),
                      ),
                    ],
                  ),
                  actions: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Notifications()));
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.notifications,
                              size: 25,
                              color: MyColor.customwhite,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Cart()));
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.shopping_cart,
                              size: 25,
                              color: MyColor.customwhite,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                  backgroundColor: MyColor.customblue,
                  bottom: AppBar(
                    automaticallyImplyLeading: false,
                    backgroundColor: MyColor.customblue,
                    title: Container(
                      width: double.infinity,
                      height: 40,
                      color: MyColor.customwhite,
                      child: Center(
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: "Search for Something...",
                              prefixIcon: Icon(Icons.search),
                              suffixIcon: Icon(Icons.camera_alt_outlined)),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 2,
                    mainAxisSpacing: 2,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailsPage(
                                        key: null,
                                        itemtitle: '${items[index].title}',
                                        itemimage: '${items[index].image}',
                                        itemdescription: '${items[index].description}',
                                        itemprice: '${items[index].price}',
                                        itemcategory: '${items[index].category}',
                                        itemrate: '${items[index].rate}',
                                        itemcount: '${items[index].count}',
                                      )));
                        },
                        child: Card(
                          elevation: 10,
                          shadowColor: MyColor.customblue,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 8, top: 5),
                                  alignment: Alignment.centerRight,
                                  child: Icon(
                                    Icons.favorite,
                                    size: 18,
                                    color: MyColor.customash,
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  height: 105,
                                  child: Image.network(items[index].image!),
                                ),
                                Container(
                                  color: Colors.grey[300],
                                  // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Container(
                                        child: RichText(
                                          text: TextSpan(
                                              text:
                                                  ("Rs ${items[index].price}"),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: MyColor.customblack,
                                                  fontSize: 12),
                                              children: [
                                                TextSpan(
                                                  text: (" 8̶0̶0̶"),
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: MyColor.customash,
                                                      fontSize: 10),
                                                ),
                                                TextSpan(
                                                  text: "  30% off",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          MyColor.customgreen,
                                                      fontSize: 10),
                                                )
                                              ]),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 20,
                                              width: 60,
                                              child: ElevatedButton.icon(
                                                onPressed: () {},
                                                icon: Container(
                                                  // width: 10,
                                                  child: Icon(
                                                    Icons.add_shopping_cart,
                                                    size: 10,
                                                  ),
                                                ),
                                                label: Text(
                                                  "Buy",
                                                  style: TextStyle(fontSize: 8),
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                    primary:
                                                        MyColor.customgreen,
                                                    shadowColor:
                                                        MyColor.customgreen,
                                                    elevation: 10,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50),
                                                    )),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 8,
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 20,
                                              width: 70,
                                              child: ElevatedButton.icon(
                                                onPressed: () {},
                                                icon: Container(
                                                  // width: 10,
                                                  child: Icon(
                                                    Icons.share_outlined,
                                                    size: 10,
                                                  ),
                                                ),
                                                label: Text(
                                                  "Share",
                                                  style: TextStyle(fontSize: 8),
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                    primary:
                                                        MyColor.customblack,
                                                    shadowColor:
                                                        MyColor.customblack,
                                                    elevation: 10,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50),
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    childCount: items.length,
                  ),
                )
              ],
            ),
    );
  }
}

class Product {
  final String? title;
  final String? price;
  final String? description;
  final String? category;
  final String? image;
  final String? rate;
  final String? count;

  Product(
      {this.title,
      this.price,
      this.description,
      this.category,
      this.image,
      this.rate,
      this.count});
}
