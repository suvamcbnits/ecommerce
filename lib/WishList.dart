import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class WishList extends StatefulWidget {
  const WishList({Key? key}) : super(key: key);

  @override
  _WishListState createState() => _WishListState();
}

class _WishListState extends State<WishList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Wishlist and Collections"),
      ),
      body: Container(
        child: ListView(
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "My Wishlist",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: [
                      Container(
                        child: Icon(
                          Icons.lock,
                          color: MyColor.customash,
                          size: 15,
                        ),
                      ),
                      Container(
                        child: Text(
                          "Private .",
                          style: TextStyle(
                              fontSize: 14,
                              color: MyColor.customash,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        child: Text(
                          "6 items",
                          style: TextStyle(
                              fontSize: 14,
                              color: MyColor.customash,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  elevation: 10,
                  shadowColor: MyColor.customblue,
                  child: Container(
                    width: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8,top: 5),
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.more_vert,
                            size: 22,
                            color: MyColor.customash,
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          height: 150,
                          child: Image.asset("image/9.jpeg"),
                        ),
                        Container(
                          width: 170,
                          color: Colors.grey[300],
                          // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              SizedBox(height: 4,),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: "Rs. 599 ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: MyColor.customblack,
                                          fontSize: 12),
                                      children: [
                                        TextSpan(
                                          text: "2̶4̶9̶9̶",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash,
                                              fontSize: 10),
                                        ),
                                        TextSpan(
                                          text: "  30% off",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customgreen,
                                              fontSize: 10),
                                        )
                                      ]
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  shadowColor: MyColor.customblue,
                  child: Container(
                    width: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8,top: 5),
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.more_vert,
                            size: 22,
                            color: MyColor.customash,
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          height: 150,
                          child: Image.asset("image/1.jpeg"),
                        ),
                        Container(
                          width: 170,
                          color: Colors.grey[300],
                          // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              SizedBox(height: 4,),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: "Rs. 1099 ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: MyColor.customblack,
                                          fontSize: 12),
                                      children: [
                                        TextSpan(
                                          text: "2̶4̶9̶9̶",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash,
                                              fontSize: 10),
                                        ),
                                        TextSpan(
                                          text: "  30% off",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customgreen,
                                              fontSize: 10),
                                        )
                                      ]
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  elevation: 10,
                  shadowColor: MyColor.customblue,
                  child: Container(
                    width: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8,top: 5),
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.more_vert,
                            size: 22,
                            color: MyColor.customash,
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          height: 150,
                          child: Image.asset("image/0.jpeg"),
                        ),
                        Container(
                          width: 170,
                          color: Colors.grey[300],
                          // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              SizedBox(height: 4,),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: "Rs. 1999 ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: MyColor.customblack,
                                          fontSize: 12),
                                      children: [
                                        TextSpan(
                                          text: "2̶4̶9̶9̶",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash,
                                              fontSize: 10),
                                        ),
                                        TextSpan(
                                          text: "  30% off",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customgreen,
                                              fontSize: 10),
                                        )
                                      ]
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  shadowColor: MyColor.customblue,
                  child: Container(
                    width: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8,top: 5),
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.more_vert,
                            size: 22,
                            color: MyColor.customash,
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          height: 150,
                          child: Image.asset("image/6.jpeg"),
                        ),
                        Container(
                          width: 170,
                          color: Colors.grey[300],
                          // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              SizedBox(height: 4,),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: "Rs. 1199 ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: MyColor.customblack,
                                          fontSize: 12),
                                      children: [
                                        TextSpan(
                                          text: "2̶4̶9̶9̶",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash,
                                              fontSize: 10),
                                        ),
                                        TextSpan(
                                          text: "  30% off",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customgreen,
                                              fontSize: 10),
                                        )
                                      ]
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  elevation: 10,
                  shadowColor: MyColor.customblue,
                  child: Container(
                    width: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8,top: 5),
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.more_vert,
                            size: 22,
                            color: MyColor.customash,
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          height: 150,
                          child: Image.asset("image/b.jpeg"),
                        ),
                        Container(
                          width: 170,
                          color: Colors.grey[300],
                          // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              SizedBox(height: 4,),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: "Rs. 999 ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: MyColor.customblack,
                                          fontSize: 12),
                                      children: [
                                        TextSpan(
                                          text: "2̶4̶9̶9̶",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash,
                                              fontSize: 10),
                                        ),
                                        TextSpan(
                                          text: "  30% off",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customgreen,
                                              fontSize: 10),
                                        )
                                      ]
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  shadowColor: MyColor.customblue,
                  child: Container(
                    width: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8,top: 5),
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.more_vert,
                            size: 22,
                            color: MyColor.customash,
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          height: 150,
                          child: Image.asset("image/a.jpeg"),
                        ),
                        Container(
                          width: 170,
                          color: Colors.grey[300],
                          // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              SizedBox(height: 4,),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: "Rs. 1699 ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: MyColor.customblack,
                                          fontSize: 12),
                                      children: [
                                        TextSpan(
                                          text: "2̶4̶9̶9̶",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash,
                                              fontSize: 10),
                                        ),
                                        TextSpan(
                                          text: "  30% off",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColor.customgreen,
                                              fontSize: 10),
                                        )
                                      ]
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),

          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(left: 5,right: 5),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(color: MyColor.customblue)
          ),
          onPressed: ()=>null,
          color: MyColor.customwhite,
          textColor: MyColor.customblue,
          child: Text('Create New Collection',style: TextStyle(fontSize: 15),),
        ),
      ),
    );
  }
}

