import 'dart:ffi';

import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'Address_Page.dart';

class Cart extends StatefulWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  int _value = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Cart"),
        elevation: 20,
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: [
            Card(
              elevation: 20,
              shadowColor: MyColor.customgreen,
              child: Container(
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            child: Image.asset("image/a.jpeg"),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: Text(
                                          "SONATA",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        child: Text(
                                          "77106 Analog Watch",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  child: RichText(
                                    text: TextSpan(
                                        text: ("₹ 1,499/-"),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: MyColor.customblack,
                                            fontSize: 16),
                                        children: [
                                          TextSpan(
                                            text: (" 1̶8̶0̶0̶"),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: MyColor.customash,
                                                fontSize: 12),
                                          ),
                                          TextSpan(
                                            text: "  30% off",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: MyColor.customgreen,
                                                fontSize: 12),
                                          )
                                        ]),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 18,
                                  width: 35,
                                  decoration: BoxDecoration(
                                      color: MyColor.customgreen,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Text(
                                          "4.2",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: MyColor.customwhite),
                                        ),
                                      ),
                                      Container(
                                        child: Icon(
                                          Icons.star,
                                          size: 15,
                                          color: MyColor.customwhite,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            width: 59,
                            decoration: BoxDecoration(
                                border: Border.all(color: MyColor.customblack)),
                            child: Row(
                              children: [
                                Container(
                                  child: Text(
                                    'Qty: 1',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ),
                                Container(
                                  color: MyColor.customwhite,
                                  child: Icon(
                                    Icons.add_outlined,
                                    size: 18,
                                    color: MyColor.customblack,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                              decoration: BoxDecoration(
                                  color: MyColor.customwhite,
                                  border:
                                      Border.all(color: MyColor.customblack),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              child: Row(
                                children: [
                                  Container(
                                    width: 30,
                                    child: Icon(
                                      Icons.local_offer_outlined,
                                      size: 20,
                                      color: MyColor.customblack,
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                        "  Apply Coupons & \n Save Upto ₹499.00"),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 30,
                      decoration: BoxDecoration(
                          color: MyColor.customwhite,
                          border: Border.all(color: MyColor.customblack),
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      child: Container(
                        child: Row(
                          children: [
                            Container(
                              width: 150,
                              padding: EdgeInsets.only(left: 20),
                              child: Row(
                                children: [
                                  Container(
                                    child: Icon(
                                      Icons.archive_rounded,
                                      size: 15,
                                      color: MyColor.customash,
                                    ),
                                  ),
                                  Container(
                                    child: Text("Save for Later"),
                                  )
                                ],
                              ),
                            ),
                            VerticalDivider(
                              width: 20,
                              color: MyColor.customblack,
                              thickness: 1,
                            ),
                            Container(
                              width: 150,
                              margin: EdgeInsets.only(left: 10),
                              child: Row(
                                children: [
                                  Container(
                                    child: Icon(
                                      Icons.delete,
                                      size: 15,
                                      color: MyColor.customblack,
                                    ),
                                  ),
                                  Container(
                                    child: Text("Remove from Cart"),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Card(
              elevation: 20,
              shadowColor: MyColor.customgreen,
              child: Container(
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            child: Image.asset("image/b.jpeg"),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: Text(
                                          "NIKE",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        child: Text(
                                          "Air Mosaic 75,Running Shoes Men(White)",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  child: RichText(
                                    text: TextSpan(
                                        text: ("₹ 1,599/-"),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: MyColor.customblack,
                                            fontSize: 16),
                                        children: [
                                          TextSpan(
                                            text: (" 2̶0̶0̶0̶"),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: MyColor.customash,
                                                fontSize: 12),
                                          ),
                                          TextSpan(
                                            text: "  45% off",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: MyColor.customgreen,
                                                fontSize: 12),
                                          )
                                        ]),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 18,
                                  width: 35,
                                  decoration: BoxDecoration(
                                      color: MyColor.customgreen,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Text(
                                          "4.0",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: MyColor.customwhite),
                                        ),
                                      ),
                                      Container(
                                        child: Icon(
                                          Icons.star,
                                          size: 15,
                                          color: MyColor.customwhite,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            width: 59,
                            decoration: BoxDecoration(
                                border: Border.all(color: MyColor.customblack)),
                            child: Row(
                              children: [
                                Container(
                                  child: Text(
                                    'Qty: 1',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ),
                                Container(
                                  color: MyColor.customash,
                                  child: Icon(
                                    Icons.add_outlined,
                                    size: 18,
                                    color: MyColor.customblack,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                              decoration: BoxDecoration(
                                  color: MyColor.customwhite,
                                  border:
                                      Border.all(color: MyColor.customblack),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              child: Row(
                                children: [
                                  Container(
                                    width: 30,
                                    child: Icon(
                                      Icons.local_offer_outlined,
                                      size: 20,
                                      color: MyColor.customblack,
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                        "  Apply Coupons & \n Save Upto ₹499.00"),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 30,
                      decoration: BoxDecoration(
                          color: MyColor.customwhite,
                          border: Border.all(color: MyColor.customblack),
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      child: Container(
                        child: Row(
                          children: [
                            Container(
                              width: 150,
                              padding: EdgeInsets.only(left: 20),
                              child: Row(
                                children: [
                                  Container(
                                    child: Icon(
                                      Icons.archive_rounded,
                                      size: 15,
                                      color: MyColor.customash,
                                    ),
                                  ),
                                  Container(
                                    child: Text("Save for Later"),
                                  )
                                ],
                              ),
                            ),
                            VerticalDivider(
                              width: 20,
                              color: MyColor.customblack,
                              thickness: 1,
                            ),
                            Container(
                              width: 150,
                              margin: EdgeInsets.only(left: 10),
                              child: Row(
                                children: [
                                  Container(
                                    child: Icon(
                                      Icons.delete,
                                      size: 15,
                                      color: MyColor.customblack,
                                    ),
                                  ),
                                  Container(
                                    child: Text("Remove from Cart"),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Card(
              elevation: 10,
              shadowColor: MyColor.customblue,
              child: Container(
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            child: Image.asset("image/c.jpeg"),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Text(
                                              "SONY 310AP ",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 60,
                                          ),
                                          Container(
                                            height: 40,
                                            width: 60,
                                            child: Image.asset(
                                              "image/best.png",
                                              fit: BoxFit.fill,
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        child: Text(
                                          "Wired HeadSet(Black,On the Ear)",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                              color: MyColor.customash),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  child: RichText(
                                    text: TextSpan(
                                        text: ("₹ 999/-"),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: MyColor.customblack,
                                            fontSize: 16),
                                        children: [
                                          TextSpan(
                                            text: (" 1̶8̶0̶0̶"),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: MyColor.customash,
                                                fontSize: 12),
                                          ),
                                          TextSpan(
                                            text: "  50% off",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: MyColor.customgreen,
                                                fontSize: 12),
                                          )
                                        ]),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 18,
                                  width: 35,
                                  decoration: BoxDecoration(
                                      color: MyColor.customgreen,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Text(
                                          "4.4",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: MyColor.customwhite),
                                        ),
                                      ),
                                      Container(
                                        child: Icon(
                                          Icons.star,
                                          size: 15,
                                          color: MyColor.customwhite,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            width: 59,
                            decoration: BoxDecoration(
                                border: Border.all(color: MyColor.customblack)),
                            child: Row(
                              children: [
                                Container(
                                  child: Text(
                                    'Qty: 1',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ),
                                Container(
                                  color: MyColor.customash,
                                  child: Icon(
                                    Icons.add_outlined,
                                    size: 18,
                                    color: MyColor.customblack,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                              decoration: BoxDecoration(
                                  color: MyColor.customwhite,
                                  border:
                                      Border.all(color: MyColor.customblack),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              child: Row(
                                children: [
                                  Container(
                                    width: 30,
                                    child: Icon(
                                      Icons.local_offer_outlined,
                                      size: 20,
                                      color: MyColor.customblack,
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                        "  Apply Coupons & \n Save Upto ₹499.00"),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 30,
                      decoration: BoxDecoration(
                          color: MyColor.customwhite,
                          border: Border.all(color: MyColor.customblack),
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      child: Container(
                        child: Row(
                          children: [
                            Container(
                              width: 150,
                              padding: EdgeInsets.only(left: 20),
                              child: Row(
                                children: [
                                  Container(
                                    child: Icon(
                                      Icons.archive_rounded,
                                      size: 15,
                                      color: MyColor.customash,
                                    ),
                                  ),
                                  Container(
                                    child: Text("Save for Later"),
                                  )
                                ],
                              ),
                            ),
                            VerticalDivider(
                              width: 20,
                              color: MyColor.customblack,
                              thickness: 1,
                            ),
                            Container(
                              width: 150,
                              margin: EdgeInsets.only(left: 10),
                              child: Row(
                                children: [
                                  Container(
                                    child: Icon(
                                      Icons.delete,
                                      size: 15,
                                      color: MyColor.customblack,
                                    ),
                                  ),
                                  Container(
                                    child: Text("Remove from Cart"),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(
              thickness: 3,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "    Payment Mode",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    children: [
                      Radio(
                        value: 1,
                        groupValue: _value,
                        onChanged: (value) {
                          setState(() {
                            _value = value as int;
                          });
                        },
                      ),
                      Text("Online Payment"),
                    ],
                  ),
                  Row(
                    children: [
                      Radio(
                        value: 2,
                        groupValue: _value,
                        onChanged: (value) {
                          setState(() {
                            _value = value as int;
                          });
                        },
                      ),
                      Text("Cash on Delivery"),
                    ],
                  )
                ],
              ),
            ),
            Divider(
              thickness: 3,
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      children: [
                        Container(
                          child: Icon(
                            Icons.local_offer,
                            color: MyColor.customgreen,
                            size: 15,
                          ),
                        ),
                        Container(
                          child: Text(
                            " Offers and Coupons",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 240,
                        margin: EdgeInsets.only(top: 10),
                        child: TextField(
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(left: 10),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              hintText: "Enter Coupon Code",
                              suffixIcon: Icon(Icons.arrow_drop_down)),
                        ),
                      ),
                      Container(
                        width: 80,
                          height: 30,
                          margin: EdgeInsets.only(top: 7),
                          child: FlatButton(color: MyColor.customblue,
                              onPressed: () {}, child: Text("Apply",style: TextStyle(color: MyColor.customwhite),)
                          )
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // Container(
            //   width: double.infinity,
            //   child: ElevatedButton(
            //     style: ElevatedButton.styleFrom(
            //       primary: MyColor.customash,
            //     ),
            //     onPressed: (){},
            //     child: Text("Checkout",style: TextStyle(color: MyColor.customblack),),
            //   ),
            // )
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(5.0),
        child: RaisedButton(
          color: MyColor.customblue,
            onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Address()));
          },
          textColor: MyColor.customblack,
          child: Text('Checkout',style: TextStyle(fontSize: 18),),
        ),
      ),
    );
  }
}
