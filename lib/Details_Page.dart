import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce/ImageShow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/widgets.dart';

import 'Cart.dart';
import 'MyCoupon.dart';
import 'components/ColorScreen.dart';

class DetailsPage extends StatefulWidget {
  const DetailsPage(
      {required key,
      required this.itemtitle,
      required this.itemprice,
      required this.itemdescription,
      required this.itemcategory,
      required this.itemimage,
      required this.itemrate,
      required this.itemcount})
      : super(key: key);
  final String itemtitle;
  final String itemprice;
  final String itemdescription;
  final String itemcategory;
  final String itemimage;
  final String itemrate;
  final String itemcount;

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  // List<Widget> generateImagesTiles() {
  //   return images
  //       .map((element) => ClipRRect(
  //             child: Image.asset(
  //               element,
  //               fit: BoxFit.cover,
  //             ),
  //             borderRadius: BorderRadius.circular(15),
  //           ))
  //       .toList();
  // }
  //
  // final List<String> images = [
  //   'image/hp_1.jpeg',
  //   'image/hp_2.jpeg',
  //   'image/hp_3.jpeg',
  //   'image/hp_4.jpeg',
  //   'image/hp_5.jpeg',
  //   'image/hp_6.jpeg',
  //   'image/hp_7.jpeg',
  //   'image/hp_8.jpeg',
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Container(
            margin: EdgeInsets.only(right: 15),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  color: MyColor.customwhite,
                ),
                SizedBox(
                  width: 15,
                ),
                Icon(
                  Icons.mic,
                  color: MyColor.customwhite,
                ),
                SizedBox(
                  width: 15,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Cart()));
                  },
                  child: Icon(
                    Icons.shopping_cart,
                    color: MyColor.customblack,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          Container(
            color: MyColor.customwhite,
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: 10),
                  child: CircleAvatar(
                    child: Icon(
                      Icons.favorite_border,
                      color: MyColor.customwhite,
                    ),
                    backgroundColor: MyColor.customblack,
                  ),
                  color: MyColor.customwhite,
                  height: 40,
                  width: double.infinity,
                ),
                Container(
                  color: MyColor.customwhite,
                  width: MediaQuery.of(context).size.width,
                  // color: Colors.white,
                  // padding: EdgeInsets.only(top: 50),
                  child: Stack(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => ImageShow(image: widget.itemimage)));
                        },
                        child: CarouselSlider(
                            items: [
                              Container(
                                // height: 300,
                                width: double.infinity,
                                child: Image.network(widget.itemimage),
                              ),
                              Container(
                                width: double.infinity,
                                child: Image.network(widget.itemimage),
                              ),
                              Container(
                                width: double.infinity,
                                child: Image.network(widget.itemimage),
                              ),
                              Container(
                                width: double.infinity,
                                child: Image.network(widget.itemimage),
                              ),
                            ],
                            options: CarouselOptions(
                              autoPlay: true,
                              enlargeCenterPage: true,
                            )),
                      ),

                      // CarouselSlider(
                      //     items: generateImagesTiles(),
                      //     options: CarouselOptions(
                      //       autoPlay: true,
                      //       enlargeCenterPage: true,
                      //     )),

                      Container(
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.only(right: 10, top: 15),
                        child: CircleAvatar(
                          child: Icon(
                            (Icons.share),
                            color: MyColor.customwhite,
                          ),
                          backgroundColor: MyColor.customblack,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  // decoration: BoxDecoration(
                  // border: Border(bottom: BorderSide(color: MyColor.customash))),
                  color: MyColor.customwhite,
                  width: double.infinity,
                  child: Column(
                    children: [
                      // Card(
                      //   margin: EdgeInsets.only(right: 15,top: 15),
                      //   // color: Colors.red,
                      //   elevation: 5,
                      //   shadowColor: Colors.white,
                      // )
                      Container(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 275),
                        child: Row(
                          children: [
                            Icon(
                              Icons.info,
                              color: MyColor.customblue,
                            ),
                            Text(
                              "Details",
                              style: TextStyle(color: MyColor.customblue),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          widget.itemtitle,
                          style: TextStyle(
                              fontSize: 18,
                              color: MyColor.customblack,
                              overflow: TextOverflow.fade,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          widget.itemdescription,
                          style: TextStyle(
                              color: MyColor.customash,
                              overflow: TextOverflow.fade,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        child: Row(
                          children: [
                            SizedBox(
                              width: 6,
                            ),
                            Container(
                              color: MyColor.customgreen,
                              child: Row(
                                children: [
                                  Text(
                                    widget.itemrate,
                                    style:
                                        TextStyle(color: MyColor.customwhite),
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: MyColor.customwhite,
                                    size: 14,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              child: Text(
                                widget.itemcount + " Ratings",
                                style: TextStyle(color: MyColor.customblack),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        alignment: Alignment.bottomLeft,
                        child: RichText(
                          text: TextSpan(
                              text: "7̶0̶0̶ ",
                              style: TextStyle(
                                color: MyColor.customblack,
                              ),
                              children: [
                                TextSpan(
                                    text: "Rs " + widget.itemprice,
                                    style: TextStyle(
                                        color: MyColor.customblack,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20)),
                                TextSpan(
                                    text: "  30% off",
                                    style: TextStyle(
                                        color: MyColor.customgreen,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15))
                              ]),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          "FREE Delivery",
                          style: TextStyle(
                            color: MyColor.customblack,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 10),
                        child: RichText(
                          text: TextSpan(
                              text: "EMI from ₹2,017/month.",
                              style: TextStyle(
                                  color: MyColor.customblack,
                                  fontWeight: FontWeight.bold),
                              children: [
                                TextSpan(
                                    text: "View Plans",
                                    style: TextStyle(
                                      color: MyColor.customblue,
                                      fontSize: 15,
                                    ))
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 5,
                  width: double.infinity,
                  color: MyColor.customash,
                ),
                Container(
                  width: 390,
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Offers & Coupons",
                          style: TextStyle(
                              color: MyColor.customblack,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.local_offer,
                              color: MyColor.customgreen,
                              size: 20,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            RichText(
                              text: TextSpan(
                                  text: "Bank Offer",
                                  style: TextStyle(
                                      color: MyColor.customblack,
                                      fontWeight: FontWeight.bold),
                                  children: [
                                    TextSpan(
                                      text:
                                          " Flat ₹50 Instant Cashback on Paytm\nWallet. Min Order Value ₹500. Valid once per\nPaytm account",
                                      style: TextStyle(
                                          color: MyColor.customblack,
                                          fontWeight: FontWeight.w600,
                                          overflow: TextOverflow.clip),
                                    ),
                                    TextSpan(
                                        text: " T&C",
                                        style: TextStyle(
                                            color: MyColor.customblue,
                                            fontWeight: FontWeight.bold))
                                  ]),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 17),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: MyColor.customblack,
                                size: 17,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.local_offer,
                              color: MyColor.customgreen,
                              size: 20,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            RichText(
                              text: TextSpan(
                                  text: "Bank Offer",
                                  style: TextStyle(
                                      color: MyColor.customblack,
                                      fontWeight: FontWeight.bold),
                                  children: [
                                    TextSpan(
                                      text:
                                          " 5% Unlimited Cashback on Flipkart \nAxis Bank Credit Card",
                                      style: TextStyle(
                                          color: MyColor.customblack,
                                          fontWeight: FontWeight.w600,
                                          overflow: TextOverflow.clip),
                                    ),
                                    TextSpan(
                                        text: " T&C",
                                        style: TextStyle(
                                            color: MyColor.customblue,
                                            fontWeight: FontWeight.bold))
                                  ]),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 22),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: MyColor.customblack,
                                size: 17,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.local_offer,
                              color: MyColor.customgreen,
                              size: 20,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            RichText(
                              text: TextSpan(
                                  text: "Partner Offer",
                                  style: TextStyle(
                                      color: MyColor.customblack,
                                      fontWeight: FontWeight.bold),
                                  children: [
                                    TextSpan(
                                      text:
                                          "   Sign up for Flipkart Pay Later and \nget Flipkart Gift Card worth ₹100*",
                                      style: TextStyle(
                                          color: MyColor.customblack,
                                          fontWeight: FontWeight.w600,
                                          overflow: TextOverflow.clip),
                                    ),
                                    TextSpan(
                                        text: " Know More",
                                        style: TextStyle(
                                            color: MyColor.customblue,
                                            fontWeight: FontWeight.bold))
                                  ]),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 8),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: MyColor.customblack,
                                size: 17,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 2,
                  width: double.infinity,
                  color: MyColor.customash,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyCoupon()));
                  },
                  child: Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Container(
                      padding: EdgeInsets.only(right: 18),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 8, left: 8),
                            child: Text(
                              "All offers & Coupons",
                              style: TextStyle(
                                color: MyColor.customblack,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: MyColor.customblack,
                              size: 18,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 2,
                  width: double.infinity,
                  color: MyColor.customash,
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: CircleAvatar(
                          backgroundColor: MyColor.customwhite,
                          child: Icon(
                            Icons.stacked_line_chart,
                            color: MyColor.customgreen,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          "680 people ordered this in the last 30 days",
                          style: TextStyle(
                            color: MyColor.customblack,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   elevation: 10,
      //
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: IconButton(onPressed: (){}, icon: Text("GO TO CART",overflow: TextOverflow.visible,),
      //
      //         alignment: Alignment.centerLeft,
      //         color: Colors.black,
      //       ),
      //       label: '',
      //       // backgroundColor: Colors.yellow,
      //     ),
      //
      //     BottomNavigationBarItem(
      //       icon: IconButton(onPressed: (){}, icon: Text("BUY NOW"),
      //       color: Colors.red,
      //       ),
      //       label: '',
      //       // backgroundColor: Colors.yellow,
      //     ),
      //   ],
      // ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              height: 45,
              // width: 195,
              child: ElevatedButton(
                style: TextButton.styleFrom(
// padding: EdgeInsets.all(0),
                    elevation: 0,
                    shadowColor: MyColor.customwhite,
                    backgroundColor: MyColor.customblack),
                onPressed: () {},
                child: Text(
                  "ADD TO CART",
                  style: TextStyle(color: MyColor.customwhite),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 45,

              // width: 195,
              child: ElevatedButton(
                style: TextButton.styleFrom(
                    shadowColor: MyColor.customwhite,
                    elevation: 0,
                    backgroundColor: MyColor.customblue),
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Cart()));
                },
                child: Text(
                  "BUY NOW",
                  style: TextStyle(color: MyColor.customwhite),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
