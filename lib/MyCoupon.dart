import 'package:flutter/material.dart';

import 'components/ColorScreen.dart';

class MyCoupon extends StatefulWidget {
  const MyCoupon({Key? key}) : super(key: key);

  @override
  _MyCouponState createState() => _MyCouponState();
}

class _MyCouponState extends State<MyCoupon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Coupons and Cards"),
      ),
      body: Container(
        child: ListView(
          children: [
            Container(
              padding: EdgeInsets.only(left: 10,top: 10),
                child: Text("Bank Offer",
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 18))
            ),
            SizedBox(height: 10,),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 40,
                      child: Icon(
                        Icons.local_offer,
                        color: MyColor.customgreen,
                        size: 20,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      margin: EdgeInsets.only(right: 20),
                      child: RichText(
                        text: TextSpan(
                            text: "Flat ",
                            style: TextStyle(
                                color: MyColor.customblack,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text:
                                    "25% Unlimited Cashback on EShop \nAxis Bank Credit Card",
                                style: TextStyle(
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w400,
                                    overflow: TextOverflow.clip),
                              ),
                              TextSpan(
                                  text: " T&C",
                                  style: TextStyle(
                                      color: MyColor.customblue,
                                      fontWeight: FontWeight.bold))
                            ]),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColor.customblack,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 40,
                      child: Icon(
                        Icons.local_offer,
                        color: MyColor.customgreen,
                        size: 20,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      margin: EdgeInsets.only(right: 20),
                      child: RichText(
                        text: TextSpan(
                            text: "Upto ",
                            style: TextStyle(
                                color: MyColor.customblack,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text:
                                "₹199 Instant Cashback on Paytm \nMin Order Value ₹500\n",
                                style: TextStyle(
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w400,
                                    overflow: TextOverflow.clip),
                              ),
                              TextSpan(
                                  text: "Valid Once Per Paytm Account",
                                  style: TextStyle(
                                      color: MyColor.customblue,
                                      fontWeight: FontWeight.bold))
                            ]),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColor.customblack,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 10,top: 10),
                child: Text("Partner Offer",
                    style:
                    TextStyle(fontWeight: FontWeight.w600, fontSize: 18))
            ),
            SizedBox(height: 10,),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 40,
                      child: Icon(
                        Icons.local_offer,
                        color: MyColor.customgreen,
                        size: 20,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      margin: EdgeInsets.only(right: 20),
                      child: RichText(
                        text: TextSpan(
                            text: "Sign up ",
                            style: TextStyle(
                                color: MyColor.customblack,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text:
                                "for E-Shop Pay Later and get\n",
                                style: TextStyle(
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w400,
                                    overflow: TextOverflow.clip),
                              ),
                              TextSpan(
                                  text: "E-Shop Gift Card worth ₹300*",
                                  style: TextStyle(
                                      color: MyColor.customblack,
                                      fontWeight: FontWeight.w500))
                            ]),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColor.customblack,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 10,top: 10),
                child: Text("No Cost EMI Offer",
                    style:
                    TextStyle(fontWeight: FontWeight.w600, fontSize: 18))
            ),
            SizedBox(height: 10,),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 40,
                      child: Icon(
                        Icons.local_offer,
                        color: MyColor.customgreen,
                        size: 20,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      margin: EdgeInsets.only(right: 20),
                      child: RichText(
                        text: TextSpan(
                            text: "No Cost EMI ",
                            style: TextStyle(
                                color: MyColor.customblack,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text:
                                "on Bajaj Fines EMI Card\n",
                                style: TextStyle(
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w400,
                                    overflow: TextOverflow.clip),
                              ),
                              TextSpan(
                                  text: "on Cart Value above ₹2999",
                                  style: TextStyle(
                                      color: MyColor.customblack,
                                      fontWeight: FontWeight.w500))
                            ]),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColor.customblack,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Card(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(child: Text("Gift Cards",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),),),
                          Container(child: Text("Rs. 0",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),),)
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      alignment: Alignment.centerRight,
                      child: Text("ADD E-SHOP GIFT CARD",
                        style: TextStyle(fontSize: 15,fontWeight: FontWeight.w400,color: MyColor.customblue),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 5,),
            Card(
              color: MyColor.customblue,
              elevation: 10,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 40,
                      child: Image.asset("image/gift.png"),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      margin: EdgeInsets.only(right: 20),
                      child: RichText(
                        text: TextSpan(
                            text: "Gift Cards",
                            style: TextStyle(
                                color: MyColor.customwhite,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text:
                                " for all Ocassions.\n",
                                style: TextStyle(
                                    color: MyColor.customwhite,
                                    fontWeight: FontWeight.w400,
                                    overflow: TextOverflow.clip),
                              ),
                              TextSpan(
                                  text: "See Options",
                                  style: TextStyle(
                                      color: MyColor.customwhite,
                                      fontWeight: FontWeight.w500))
                            ]),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColor.customwhite,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 5,),
            Card(
              color: MyColor.customblue,
              elevation: 10,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 40,
                      child: Image.asset("image/cash.png"),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      margin: EdgeInsets.only(right: 20),
                      child: RichText(
                        text: TextSpan(
                            text: "E-SHOP",
                            style: TextStyle(
                                color: MyColor.customblack,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text:
                                " Pay Later\n",
                                style: TextStyle(
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w400,
                                    overflow: TextOverflow.clip),
                              ),
                              TextSpan(
                                  text: "Sign Up and Get Rs. 100* Gift Card",
                                  style: TextStyle(
                                      color: MyColor.customblack,
                                      fontWeight: FontWeight.w400))
                            ]),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColor.customblack,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
