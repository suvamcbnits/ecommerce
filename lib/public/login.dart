import 'dart:async';
import 'dart:ffi';
import 'package:ecommerce/AccountInfo.dart';
import 'package:ecommerce/components/ColorScreen.dart';
import 'package:ecommerce/layout.dart';
import 'package:ecommerce/public/forgot_password.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum MobileVerificationState {
  Show_mobile_form_state,
  Show_OTP_form_state,
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  MobileVerificationState currentState =
      MobileVerificationState.Show_mobile_form_state;

  final phoneController = TextEditingController();
  final otpController = TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;
  String? verificationId;
  bool showLoading = false;
  int start = 30;
  bool wait=false;
  void signInWithPhoneAuthCredential(
      PhoneAuthCredential phoneAuthCredential) async {
    setState(() {
      showLoading = true;
    });
    try {
      final authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);

      setState(() {
        showLoading = false;
      });

      if (authCredential.user != null) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => AccountInfo()));
        Fluttertoast.showToast(
            msg: "Successfully Logged In",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } on FirebaseAuthException catch (e) {
      setState(() {
        showLoading = false;
      });
      _scaffoldkey.currentState!
          .showSnackBar(SnackBar(content: Text(e.message.toString())));
    }
  }

  void startTimer() {
    const onsec = Duration(seconds: 1);
    Timer timer = Timer.periodic(onsec, (timer) {
      if (start == 0) {
        setState(() {
          timer.cancel();
        });
      } else {
        setState(() {
          start--;
        });
      }
    });
    start=30;
  }

  getMobileFormWidget(context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 50),
            alignment: Alignment.centerLeft,
            child: Text(
              "Log In for the Best Experience ",
              style: TextStyle(
                  color: MyColor.customblack,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Experience the all new Ecommerce!",
              style: TextStyle(
                  color: MyColor.customash,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: 38),
          Container(
            // padding: EdgeInsets.only(left: 20, right: 20),
            child: TextFormField(
              keyboardType: TextInputType.number,
              controller: phoneController,
              decoration: InputDecoration(
                isDense: true,
                prefixIcon: Padding(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Text("+91  ")),
                // prefixIconConstraints: BoxConstraints(
                //   minWidth: 0,maxHeight: 0
                // ),
                labelText: "Phone Number",
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: MyColor.customblack),
                    borderRadius: BorderRadius.circular(10)),
                // suffixIcon: Icon(Icons.phone),
              ),
            ),
          ),
          Container(
            // alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: TextButton(
                    onPressed: () async {
                      setState(() {
                        showLoading = true;
                      });
                      await _auth.verifyPhoneNumber(
                        phoneNumber: ("+91") + (phoneController.text),
                        verificationCompleted: (phoneAuthCredential) async {
                          setState(() {
                            showLoading = false;
                            Fluttertoast.showToast(
                                msg: "OTP SENT",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.blue,
                                textColor: Colors.black,
                                fontSize: 16.0);
                          });
                          // signInWithPhoneAuthCredential(phoneAuthCredential);
                        },
                        verificationFailed: (verificationFailed) async {
                          setState(() {
                            showLoading = false;
                          });
                          _scaffoldkey.currentState!.showSnackBar(SnackBar(
                              content:
                                  Text(verificationFailed.message.toString())));
                        },
                        codeSent: (verficationId, resendingToken) async {
                          // print("senttttttttt");
                          // print(verficationId.toString());
                          setState(() {
                            showLoading = false;
                            currentState =
                                MobileVerificationState.Show_OTP_form_state;
                            verificationId = verficationId;
                          });
                        },
                        codeAutoRetrievalTimeout: (verificationId) async {},
                      );
                    },
                    child: Text("Send OTP"),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
              child: RichText(
            text: TextSpan(
              text: "By continuing,You agree to Ecommerce's",
              style: TextStyle(
                color: MyColor.customblack,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
              children: [
                TextSpan(
                    text: "Terms of Use",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: MyColor.customblue)),
                TextSpan(
                  text: " and ",
                  style: TextStyle(
                    color: MyColor.customblack,
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                  ),
                ),
                TextSpan(
                    text: "Privacy Policy",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: MyColor.customblue)),
              ],
            ),
          ))
        ],
      ),
    );
  }

  getOtpFormWidget(context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 50),
            alignment: Alignment.centerLeft,
            child: Text(
              "Log In for the Best Experience ",
              style: TextStyle(
                  color: MyColor.customblack,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Experience the all new Ecommerce!",
              style: TextStyle(
                  color: MyColor.customash,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: 38),
          Container(
            // padding: EdgeInsets.only(left: 20, right: 20),
            child: TextFormField(
              keyboardType: TextInputType.number,
              controller: phoneController,
              decoration: InputDecoration(
                isDense: true,
                prefixIcon: Padding(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Text("+91  ")),
                // prefixIconConstraints: BoxConstraints(
                //   minWidth: 0,maxHeight: 0
                // ),
                labelText: "Phone Number",
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: MyColor.customblack),
                    borderRadius: BorderRadius.circular(10)),
                // suffixIcon: Icon(Icons.phone),
              ),
            ),
          ),
          Container(
            // alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: TextButton(
                    onPressed: () async {
                      setState(() {
                        // showLoading = true;
                        startTimer();
                      });
                      await _auth.verifyPhoneNumber(
                        phoneNumber: ("+91") + (phoneController.text),
                        verificationCompleted: (phoneAuthCredential) async {
                          setState(() {
                            showLoading = false;
                            Fluttertoast.showToast(
                                msg: "OTP SENT",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.blue,
                                textColor: Colors.black,
                                fontSize: 16.0);
                          });
                          // signInWithPhoneAuthCredential(phoneAuthCredential);
                        },
                        verificationFailed: (verificationFailed) async {
                          setState(() {
                            showLoading = false;
                          });
                          _scaffoldkey.currentState!.showSnackBar(SnackBar(
                              content:
                                  Text(verificationFailed.message.toString())));
                        },
                        codeSent: (verficationId, resendingToken) async {
                          // print("senttttttttt");
                          // print(verficationId.toString());
                          setState(() {
                            showLoading = false;
                            // currentState =
                            //     MobileVerificationState.Show_OTP_form_state;
                            verificationId = verficationId;
                          });
                        },
                        codeAutoRetrievalTimeout: (verificationId) async {},
                      );
                    },
                    child: Text("Resend OTP"),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: RichText(
                text: TextSpan(
              children: [
                TextSpan(
                  text: "Send OTP again in ",
                  style: TextStyle(
                      fontSize: 15,
                      color: MyColor.customblue,
                      fontWeight: FontWeight.w500),
                ),
                TextSpan(
                  text: "00:$start",
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.red,
                      fontWeight: FontWeight.w500),
                ),
                TextSpan(
                  text: " sec",
                  style: TextStyle(
                      fontSize: 15,
                      color: MyColor.customblue,
                      fontWeight: FontWeight.w500),
                )
              ],
            )),
          ),
          SizedBox(height: 10,),
          Container(
            alignment: Alignment.bottomCenter,
            // height: 300,
            child: TextFormField(
              controller: otpController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                isDense: true,
                //   minWidth: 0,maxHeight: 0
                // ),
                labelText: "OTP..",
                hintText: "Enter OTP",
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: MyColor.customblack),
                    borderRadius: BorderRadius.circular(10)),
                // suffixIcon: Icon(Icons.phone),
              ),
            ),
          ),
          Container(
            child: ElevatedButton(
              onPressed: () async {
                // print("aaaaaaaaaaaaaaa");
                // print( otpController.text);
                // print(verificationId.toString());
                PhoneAuthCredential phoneAuthCredential =
                    PhoneAuthProvider.credential(
                        verificationId: verificationId.toString(),
                        smsCode: otpController.text);
                signInWithPhoneAuthCredential(phoneAuthCredential);
              },
              child: Text("Log in"),
            ),
          ),
          Container(
              child: RichText(
            text: TextSpan(
              text: "By continuing,You agree to Ecommerce's",
              style: TextStyle(
                color: MyColor.customblack,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
              children: [
                TextSpan(
                    text: "Terms of Use",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: MyColor.customblue)),
                TextSpan(
                  text: " and ",
                  style: TextStyle(
                    color: MyColor.customblack,
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                  ),
                ),
                TextSpan(
                    text: "Privacy Policy",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: MyColor.customblue)),
              ],
            ),
          ))
        ],
      ),
    );
  }

  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldkey,
        appBar: AppBar(
          // actions: [ElevatedButton(onPressed: (){
          //   Navigator.pop(context);
          // }, child: Icon(Icons.arrow_back))],
          title: Text("Log In"),
        ),
        body: showLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : currentState == MobileVerificationState.Show_mobile_form_state
                ? getMobileFormWidget(context)
                : getOtpFormWidget(context)

        // Container(
        //   padding: EdgeInsets.only(left: 20, right: 20),
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     // crossAxisAlignment: CrossAxisAlignment.stretch,
        //     children: [
        //       Container(
        //         margin: EdgeInsets.only(top: 50),
        //         alignment: Alignment.centerLeft,
        //         child: Text(
        //           "Log In for the Best Experience ",
        //           style: TextStyle(
        //               color: MyColor.customblack,
        //               fontWeight: FontWeight.bold,
        //               fontSize: 20),
        //         ),
        //       ),
        //       SizedBox(
        //         height: 20,
        //       ),
        //       Container(
        //         alignment: Alignment.centerLeft,
        //         child: Text(
        //           "Experience the all new Ecommerce!",
        //           style: TextStyle(
        //               color: MyColor.customash,
        //               fontSize: 15,
        //               fontWeight: FontWeight.w500),
        //         ),
        //       ),
        //       SizedBox(height: 38),
        //       Container(
        //         // padding: EdgeInsets.only(left: 20, right: 20),
        //         child: TextFormField(
        //           keyboardType: TextInputType.number,
        //           decoration: InputDecoration(
        //             isDense: true,
        //             prefixIcon: Padding(
        //                 padding: EdgeInsets.only(top: 15, left: 10, right: 10),
        //                 child: Text("+91  ")),
        //             // prefixIconConstraints: BoxConstraints(
        //             //   minWidth: 0,maxHeight: 0
        //             // ),
        //             labelText: "Phone Number",
        //             border: OutlineInputBorder(
        //                 borderSide: BorderSide(color: MyColor.customblack),
        //                 borderRadius: BorderRadius.circular(10)),
        //             // suffixIcon: Icon(Icons.phone),
        //           ),
        //         ),
        //       ),
        //       Container(
        //         // alignment: Alignment.center,
        //         child: Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           children: [
        //             // GestureDetector(
        //             //   onTap: () {
        //             //     Navigator.push(
        //             //         context,
        //             //         MaterialPageRoute(
        //             //             builder: (context) => ForgotPassword()));
        //             //   },
        //             //   child: Text("Forgot Password"),
        //             // ),
        //             Container(
        //               child: TextButton(
        //                 onPressed: () {
        //                   setState(() {
        //                     isVisible = !isVisible;
        //                   });
        //                 },
        //                 child: Text("Send OTP"),
        //               ),
        //             ),
        //           ],
        //         ),
        //       ),
        //       SizedBox(
        //         height: 5,
        //       ),
        //       Column(
        //         children: [
        //           Visibility(
        //             maintainState: true,
        //             maintainAnimation: true,
        //             maintainSize: true,
        //             visible: isVisible,
        //             child: Container(
        //               // padding: EdgeInsets.all(5),
        //               child: TextFormField(
        //                 keyboardType: TextInputType.number,
        //                 decoration: InputDecoration(
        //                   isDense: true,
        //                   //   minWidth: 0,maxHeight: 0
        //                   // ),
        //                   labelText: "OTP....",
        //                   border: OutlineInputBorder(
        //                       borderSide: BorderSide(color: MyColor.customblack),
        //                       borderRadius: BorderRadius.circular(10)),
        //                   // suffixIcon: Icon(Icons.phone),
        //                 ),
        //               ),
        //             ),
        //           ),
        //           Visibility(
        //             maintainState: true,
        //             maintainAnimation: true,
        //             maintainSize: true,
        //             visible: isVisible,
        //             child: Container(
        //               child: ElevatedButton(
        //                 onPressed: () {
        //                   Navigator.push(
        //                       context,
        //                       MaterialPageRoute(
        //                           builder: (context) => LayoutPage()));
        //                 },
        //                 child: Text("Log in"),
        //               ),
        //             ),
        //           ),
        //         ],
        //       ),
        //       Container(
        //           child: RichText(
        //         text: TextSpan(
        //           text: "By continuing,You agree to Ecommerce's",
        //           style: TextStyle(
        //             color: MyColor.customblack,
        //             fontWeight: FontWeight.bold,
        //             fontSize: 12,
        //           ),
        //           children: [
        //             TextSpan(
        //                 text: "Terms of Use",
        //                 style: TextStyle(
        //                     fontWeight: FontWeight.bold,
        //                     color: MyColor.customblue)),
        //             TextSpan(
        //               text: " and ",
        //               style: TextStyle(
        //                 color: MyColor.customblack,
        //                 fontWeight: FontWeight.bold,
        //                 fontSize: 12,
        //               ),
        //             ),
        //             TextSpan(
        //                 text: "Privacy Policy",
        //                 style: TextStyle(
        //                     fontWeight: FontWeight.bold,
        //                     color: MyColor.customblue)),
        //           ],
        //         ),
        //       ))
        //     ],
        //   ),
        // ),
        );
  }
}
