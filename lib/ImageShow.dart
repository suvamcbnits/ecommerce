
import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
class ImageShow extends StatefulWidget {
  const ImageShow({Key? key, required this.image}) : super(key: key);
  final String image;

  @override
  _ImageShowState createState() => _ImageShowState();
}

class _ImageShowState extends State<ImageShow> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: MyColor.customwhite,
        actions: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: MyColor.customwhite,elevation: 0),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.clear,color: MyColor.customblack,size: 30,),
            ),
          )
        ],
      ),
      body: Center(
        child: Container(
          width: double.infinity,
          child: PhotoView(
            imageProvider: NetworkImage(widget.image),
            enableRotation: true,
            backgroundDecoration: BoxDecoration(color: MyColor.customwhite),
          ),
        ),
      ),
    );
  }
}
