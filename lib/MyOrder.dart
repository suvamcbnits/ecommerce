import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/material.dart';

class MyOrder extends StatefulWidget {
  const MyOrder({Key? key}) : super(key: key);

  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Orders"),
        // actions: [
        //   Container(
        //     child: Icon(Icons.search,color: MyColor.customwhite,),
        //   ),
        //   SizedBox(width: 20,),
        //   // Container(),
        // ],
      ),
      body: Container(
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              padding: EdgeInsets.only(top: 5),
              child: Row(
                children: [
                  Container(
                    width: 270,
                    height: 40,
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          hintText: "Search Your Order here",
                          suffixIcon: Icon(Icons.search_outlined)),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      showModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return Container(
                              padding: EdgeInsets.fromLTRB(12, 10, 10, 0),
                              height: 290,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Text(
                                      "Filters",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    child: Text(
                                      " Order Status",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Card(
                                    elevation: 10,
                                    child: Container(
                                      width: 105,
                                      child: Row(
                                        children: [
                                          Text(
                                            "On the Way",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                          Icon(Icons.add_outlined)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Card(
                                    elevation: 10,
                                    child: Container(
                                      width: 93,
                                      child: Row(
                                        children: [
                                          Text(
                                            "Delivered",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                          Icon(Icons.add)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider(
                                    color: MyColor.customblue,
                                  ),
                                  Container(
                                    child: Text(
                                      " Order Time",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Card(
                                    elevation: 10,
                                    child: Container(
                                      width: 120,
                                      child: Row(
                                        children: [
                                          Text(
                                            "Last 30 Days",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                          Icon(Icons.add_outlined)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Card(
                                    elevation: 10,
                                    child: Container(
                                      width: 85,
                                      child: Row(
                                        children: [
                                          Text(
                                            "Older",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                          Icon(Icons.add_outlined)
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 12,),
                                  Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          height: 45,
                                          // width: 195,
                                          child: ElevatedButton(
                                            style: TextButton.styleFrom(
                                                elevation: 5,
                                                shadowColor: MyColor.customash,
                                                backgroundColor: MyColor.customwhite),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                            child: Text(
                                              "Cancel",
                                              style: TextStyle(color: MyColor.customblue),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          height: 45,
                                          child: ElevatedButton(
                                            style: TextButton.styleFrom(
                                                shadowColor: MyColor.customash,
                                                elevation: 5,
                                                backgroundColor: MyColor.customblue),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text(
                                              "Apply",
                                              style: TextStyle(color: MyColor.customwhite),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            );
                          });
                    },
                    child: Row(
                      children: [
                        Container(
                          child: Icon(Icons.filter_list),
                        ),
                        Container(
                          child: Text("Filter"),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Divider(
              thickness: 2,
            ),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 45,
                          child: Image.asset("image/16.jpeg"),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          margin: EdgeInsets.only(right: 20),
                          child: RichText(
                            text: TextSpan(
                                text: "Delivered on Jan 25\n",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w600),
                                children: [
                                  TextSpan(
                                    text: "WildCraft Large 30L Backpack..",
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: MyColor.customash,
                                        fontWeight: FontWeight.w300,
                                        overflow: TextOverflow.clip),
                                  ),
                                ]),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 3),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: MyColor.customblack,
                            size: 20,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customash,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customash,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customash,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customash,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customash,
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text("Rate this Product now"),
                    )
                  ],
                ),
              ),
            ),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 45,
                          child: Image.asset("image/c.jpeg"),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          margin: EdgeInsets.only(right: 20),
                          child: RichText(
                            text: TextSpan(
                                text: "Will be delivered by Feb 10\n",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w600),
                                children: [
                                  TextSpan(
                                    text: "SONY 310AP wired Headset...",
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: MyColor.customash,
                                        fontWeight: FontWeight.w300,
                                        overflow: TextOverflow.clip),
                                  ),
                                ]),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 3),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: MyColor.customblack,
                            size: 20,
                          ),
                        )
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Track Your Order",
                        style: TextStyle(
                            fontSize: 14,
                            color: MyColor.customblue,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 45,
                          child: Image.asset("image/3.jpeg"),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          margin: EdgeInsets.only(right: 20),
                          child: RichText(
                            text: TextSpan(
                                text: "Delivered on Jan 15\n",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: MyColor.customblack,
                                    fontWeight: FontWeight.w600),
                                children: [
                                  TextSpan(
                                    text: "Poco X3 Pro (Blue,128GB)(RAM...",
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: MyColor.customash,
                                        fontWeight: FontWeight.w300,
                                        overflow: TextOverflow.clip),
                                  ),
                                ]),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 3),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: MyColor.customblack,
                            size: 20,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customgreen,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customgreen,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customgreen,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customgreen,
                        ),
                        Icon(
                          Icons.star,
                          size: 20,
                          color: MyColor.customash,
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Write a Review",
                        style:
                            TextStyle(fontSize: 16, color: MyColor.customblue),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
