import 'package:ecommerce/Electronics.dart';
import 'package:ecommerce/Jewelery.dart';
import 'package:ecommerce/Men.dart';
import 'package:ecommerce/Women.dart';
import 'package:ecommerce/components/ColorScreen.dart';
import 'package:flutter/material.dart';

class ProductList extends StatefulWidget {
  const ProductList({Key? key}) : super(key: key);

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Products Category"),
          bottom: TabBar(
            tabs: [
              Tab(text: "Men's Clothing"),
              Tab(text: "Women's Clothing",),
              Tab(text: "Jewelery",),
              Tab(text: "Electronics",),
            ],
          ),
        ),
        body: TabBarView(
            children: [
              Men(),
              Women(),
              Jewelery(),
              Electronics(),
            ]),
      ),
    );
  }
}
